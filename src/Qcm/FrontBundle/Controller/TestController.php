<?php

namespace Qcm\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Qcm\FrontBundle\Entity\UtilisateurRepository;

/**
 * Description of TestController
 *
 * @author asouchet2015
 */
class TestController extends Controller {
    
    public function indexAction() {
        
        $rep = $this->getDoctrine()->getRepository('QcmFrontBundle:Utilisateur');
        $user = $rep->getAutorisations(1);
        
        var_dump($user);   
        
    }
    
    
    
}
