<?php

namespace Qcm\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Qcm\BackBundle\Form\UtilisateurType;

class DefaultController extends Controller
{     
    public function indexAction()
    {
        return $this->render('QcmFrontBundle:Default:index.html.twig');
    }
    
    public function inscriptionAction(Request $request) {
        
        $form = $this->creerInscriptionForm();
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            
            $this->ajouterUtilisateur($form);
            
            return $this->redirect($this->generateUrl('index'));
            
        }
                
        $template = 'QcmFrontBundle::inscription.html.twig';
        $options = array('form' => $form->createView());
        
        return $this->render($template, $options);
        
    }
    
    
    private function creerInscriptionForm() {
        
        $form = $this->createForm(new UtilisateurType());
        
        $validerOptions = array(
            'label' => 'Valider'
        );
        $form->add('valider', 'submit', $validerOptions);
        
        return $form;
        
    }
    
    private function ajouterUtilisateur($form) {
        
        echo '<h1>Ajout utilisateur</h1>';
        // Création d'un Entity Manager
        $em = $this->getDoctrine()->getManager();
        
        // Création de l'objet utilisateur saisi et requête d'insertion
        $utilisateur = $form->getData();
        
        //var_dump($utilisateur);
        
        // Récupération du profil associé
        $profil = $form->get('profil')->getData()['undefined'];
        $utilisateur->setProfil($profil);
        $profil->setUtilisateur($utilisateur);
        
        $em->persist($utilisateur);
        
        
        
        
        // Commit des requête éffectuées
        $em->flush();
        
        
    }  
    
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                