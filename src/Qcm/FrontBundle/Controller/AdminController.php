<?php

namespace Qcm\FrontBundle\Controller;

use Qcm\FrontBundle\Controller\PageController;

/**
 * Description of AdminIndexController
 * Gestion de la partie administration de l'application
 *
 * @author asouchet2015
 */
class AdminController extends PageController {
    
    public function __construct() {
        
        parent::__construct('index');
        
    }

    public function indexAction() {
        
        
        $template = 'QcmFrontBundle:Admin:index.html.twig';
        $options = array(
            'page' => $this->getPage()
        );
        
        return $this->render($template, $options);
        
    }
    
    
    
    
    
    
}
