<?php

namespace Qcm\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Description of Tools
 *
 * @author Alain
 */
class ToolsController extends Controller {

    public static function getTabPageActive($page) {

        $tabPageActive = array(
            'index' => ($page == 'index') ? 'active' : '',
            'questions' => ($page == 'questions') ? 'active' : '',
            'themes' => ($page == 'themes') ? 'active' : '',
            'tests' => ($page == 'tests') ? 'active' : '',
            'inscriptions' => ($page == 'inscriptions') ? 'active' : ''
        );

        return $tabPageActive;
    }

}
