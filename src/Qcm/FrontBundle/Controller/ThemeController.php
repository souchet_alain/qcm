<?php

namespace Qcm\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Qcm\FrontBundle\Entity\Theme;

/**
 * Description of ThemeController
 *
 * @author asouchet2015
 */
class ThemeController extends Controller {
    
    /**
     * Affiche la liste des thèmes disponibles en base 
     * et permet à l'utilisateur de saisir un nouveau thème
     */
    public function listAction(Request $request) {
        
        // Création du formulaire de saisie d'un nouveau thème
        $themeForm = $this->creerThemeForm();
        
        // Gestion de la soumission du formulaire d'ajout de thème
        $themeForm->handleRequest($request);
        
        if ($themeForm->isValid()) {
            
            $this->ajouterTheme($themeForm->get('saisieTheme')->getData());
            
        }
        
        // Récupération de la liste des thèmes disponibles en base
        $repTheme = $this->getDoctrine()->getRepository('QcmFrontBundle:Theme');
        $themes = $repTheme->getThemesOrdered();
        
        // Création du rendu
        $template = 'QcmFrontBundle:Theme:list.html.twig';
        $options = array('themeForm'=>$themeForm->createView(), 
                            'themes'=>$themes);
        
        return $this->render($template, $options);
        
    }
    
    /**
     * Affiche les informations relatives à un thème de question
     * @param Theme $theme Thème pour lequel on veut afficher les informations
     */
    public function supprAction(Theme $theme) {
        
        $emTheme = $this->getDoctrine()->getManager();
        $emTheme->remove($theme);
        $emTheme->flush();
        
        return $this->redirect($this->generateUrl('theme_list'));
        
    }
    
    /**
     * Affiche les informations relatives à un thème de question
     * @param Theme $theme Thème pour lequel on veut afficher les informations
     */
    public function viewAction(Theme $theme) {
        
        
        
    }
    
    /**
     * Création d'un formulaire d'ajout de thème dans la base de données
     */
    protected function creerThemeForm() {
        
        // Définition des options des éléments du formulaire
        $optionsText = array('label'=>'Thème : ', 
                                'required'=>true);
        $optionsSubmit = array('label'=>'Ajouter');
        
        // Création du formulaire
        $form = $this->createFormBuilder()
                    ->add('saisieTheme', 'text', $optionsText)
                    ->add('ajouter', 'submit', $optionsSubmit)
                    ->getForm();
        
        return $form;
        
    }
    
    /**
     * Création d'un formulaire de recherche de thème
     */
    protected function creerRechercheForm() {
        
        // Définition des options des éléments du formulaire
        $optionsText = array('label'=>'', 
                                'attr'=>array('placeholder'=>'Recherche'));
        $optionsSubmit = array('label'=>'Rechercher');
        
        // Création et renvoi du formulaire
        $form = $this->createFormBuilder()
                    ->add('saisieRecherche', 'text', $optionsText)
                    ->add('rechercher', 'submit', $optionsSubmit)
                    ->getForm();
     
        return $form;
        
    }
    
    protected function ajouterTheme($libelleTheme) {
        
        // Création d'un nouveau thème
        $theme = new Theme();
        $theme->setLibelle($libelleTheme);
        
        $emTheme = $this->getDoctrine()->getManager();
        $emTheme->persist($theme);
        $emTheme->flush();
        
    }
    
    
    
}
