<?php

namespace Qcm\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Description of PageController
 *
 * @author Alain
 */
class PageController extends Controller {
    
    private $page;
    
    public function __construct($page) {
        
        $this->page = $page;
        
    }

    public function getPage() {
        return $this->page;
    }

    public function setPage($page) {
        $this->page = $page;
        return $this;
    } 
    
}
