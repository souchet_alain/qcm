<?php

namespace Qcm\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityRepository;

use Qcm\FrontBundle\Entity\Question;
use Qcm\FrontBundle\Form\QuestionType;
use Qcm\FrontBundle\Form\ReponseProposeeType;
use Qcm\FrontBundle\Entity\ReponseProposee;


/**
 * Description of QuestionsController
 *
 * @author asouchet2015
 */
class QuestionController extends Controller {
    
    public function addAction(Request $request) {
        
        //Création du formulaire de saisie d'une nouvelle question
        //$questionForm = $this->creerQuestionForm(1);
        
        // Gestion de la soumission du formulaire
        //$questionForm->handleRequest($request);
        
        /*
         * Test de la gestion des formulaires imbriqués
         * 
         */
        
        $questionForm = $this->createForm(new QuestionType());
        
        
        if ($questionForm->isValid()) {
            
            $this->ajouterQuestion($questionForm);
            
        }
        
        // Affichage du rendu de la page
        $template = 'QcmFrontBundle:Question:add.html.twig';
        $options = array('form'=>$questionForm->createView());
        
        return $this->render($template, $options);
        
        
    }
    
    /**
     * Création du formulaire de saisie d'une nouvelle question
     */
    protected function creerQuestionForm() {
 
        // Définition des options des éléments
        $optionsEnnonce = array('label'=>'Enoncé : ', 
                                'attr'=>array('placeholder'=>'Saisir un énoncé'));
        $optionsMedia = array('label'=>'Attachement : ', 
                                'required'=>false);
        $optionsSupprMedia = array('label'=>'Supprimer');
        $optionsTheme = array('label'=>'Thème : ', 
                                'class'=>'QcmFrontBundle:Theme', 
                                'query_builder'=>function(EntityRepository $repository) {
                                    return $repository->createQueryBuilder('t')
                                            ->orderBy('t.libelle', 'ASC');
                                });
        $optionsChoix = array('label'=>'Choix : ', 
                                'choices'=>array('simple'=>'Simple', 'multiple'=>'Multiple'), 
                                'multiple'=>false, 
                                'expanded'=>true, 
                                'data'=>'simple');
        
        // Création et renvoi du formulaire
        $form = $this->createFormBuilder() 
                        ->add('enonce', 'text', $optionsEnnonce)
                        ->add('media', 'file', $optionsMedia)
                        ->add('supprMedia', 'button', $optionsSupprMedia)
                        ->add('theme', 'entity', $optionsTheme)
                        ->add('choix', 'choice', $optionsChoix)
                        ->getForm();
        
        return $form;
        
    }
    
    /**
     * Ajout d'une question en base avec les données saisies dans le formulaire
     * @param type $form Formulaire de saisie d'une question
     */
    protected function ajouterQuestion($form) {
        
        // Création de la question
        $question = new Question();
        
        $question->setEnonce($form->get('enonce')->getData());
        $question->setMedia($form->get('media')->getData());
        $question->setTheme($form->get('theme')->getData());
        $question->setType(($form->get('choix')->getData() == 'simple') ? 0 : 1);
        
        // ajout en base de la nouvelle question
        $emQuestion = $this->getDoctrine()->getManager();
        $emQuestion->persist($question);
        $emQuestion->flush();
        
    }
    
    /**
     * Création d'un formulaire d'ajout de réponse 
     * @param type $numReponse Numéro de la réponse à saisir
     * @return type Formulaire de saisie d'une réponse
     */
    protected function creerReponseForm($numReponse) {
        
        // Définition des options des champs des formulaire
        $optionsLibelle = array('label'=>'Réponse ' . $numReponse);
        $optionsSupprReponse = array('label'=>'Supprimer');
        $optionsReponseCorrecte = array('label'=>'Réponse correcte');
        $optionsValider = array('label'=>'Valider');
        
        // Création et renvoi du formulaire
        $form = $this->createFormBuilder()
                    ->add('libelle', 'text', $optionsLibelle)
                    ->add('supprReponse', 'button', $optionsSupprReponse)
                    ->add('reponseCorrecte', 'checkbox', $optionsReponseCorrecte)
                    ->add('valider', 'submit', $optionsValider)
                    ->getForm();
        
        return $form;
        
    }
    
    
    
    
    
    
}
