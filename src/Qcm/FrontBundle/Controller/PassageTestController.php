<?php

namespace Qcm\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;

// Entités
use Qcm\BackBundle\Entity\Utilisateur;
use Qcm\BackBundle\Entity\QuestionTirage;
use Qcm\BackBundle\Entity\Inscription;
use Qcm\BackBundle\Entity\Test;
use Qcm\BackBundle\Entity\Question;
use Qcm\BackBundle\Entity\Profil;

class PassageTestController extends Controller
{         
    
    /* ======================================================================================== */
    /* **************************INTERFACE ACCUEIL PASSAGE DES TESTS*************************** */   
    /* ======================================================================================== */
    
    
    public function accueilPassageAction()
    {
        
        // Récupération de l'utilisateur courant
        $security = $this->get('security.context');
        $token = $security->getToken();
        $profil = $token->getUser();

        /* Permet de récupérer la liste des utilisateurs */
        $em = $this->getDoctrine()->getManager();
        
        $repository = $em->getRepository('QcmBackBundle:Inscription');
        
        // On récupère les informations relatives à un utilisateur
        $inscriptions = $repository->getInscriptionsCandidat($profil->getUtilisateur());
        
        $args = array(
                'inscriptions' => $inscriptions,
                'utilisateur' => $profil->getUtilisateur()
            );
        
        return $this->render('QcmFrontBundle:Candidat:accueilPassageTest.html.twig', $args);
    }
    
    
    /* ======================================================================================== */
    /* *******************INTERFACE ACCUEIL PAGE DE CONFIRMATION (TEMPORAIRE)****************** */   
    /* ======================================================================================== */
    
    public function pageConfirmationAction(Inscription $inscription, Test $test)
    {                   
        // A changer car les questions sont générées en arrivant sur la page et pas lorsque l'on click sur "oui"
        // On génère une liste de questions
        $this->genererQuestion($inscription, $test);
        
        
        $args = array(
                'inscription' => $inscription,
                'test' => $test
            );
        
        return $this->render('QcmFrontBundle::pageConfirmation.html.twig', $args);
    }
    
    
    /* ======================================================================================== */
    /* *****************************INTERFACE PASSAGE DES TESTS******************************** */   
    /* ======================================================================================== */
    
    public function passageTestAction(Inscription $inscription, Test $test, Request $request)
    {   
       
        // On récupère un tableau de questions
        $tabQuestions = $this->getTableauQuestions();       
        
        // On récupère l'identifiant de la question dans l'url
        $idQuestion = filter_input(INPUT_GET,'quest');       
        
        // On récupère la question correspondant à cet identifiant
        $question = $tabQuestions[$idQuestion]->getQuestion();              
               
        $formQuestion = $this->getFormQuestion($question);
        
        $formQuestion->handleRequest($request);
         
        // Si l'on appuie sur le bouton suivant, on affiche la question suivante
        if ($formQuestion->isValid()){
            
            $args = array(
                    'inscription' => $inscription->getId(),
                    'test' => $test->getId(),
                    'quest' => $idQuestion +1
                );
                        
            return $this->redirect($this->generateUrl('passageTest', $args));
        }
        
        $args = array(
                    'inscription' => $inscription,
                    'enonceQuestion' => $question->getEnonce(),
                    'formQuestion' => $formQuestion->createView()
                );

        return $this->render('QcmFrontBundle:Candidat:passageTest.html.twig', $args);
    }
    
    // Permet de stocker dans la table QuestionTirage les questions tirées
    protected function genererQuestion($inscription, $test)
    {      
        $em = $this->getDoctrine()->getManager();
        
        // On récupère toutes les informations relatives aux sections
        $sections = $test->getSections();
        
         foreach($sections as $section){
            // On récupère toutes les informations relatives à un thème
            $theme = $section->getTheme();         
            
            // On récupère toutes les informations relatives aux questions
            $questions = $theme->getQuestions();   

            for($i=0;$i<$section->getNbQuestions();$i++){
                
            
                // On génère un nombre aléatoire de questions
                $questionsAleatoires = rand(1, count($questions))-1;

                // On récupère toutes les informations relatives à une question précise
                $question = $theme->getQuestions()[$questionsAleatoires];
            
                // On créé une nouvelle entité QuestionTirage
                $questionTirage = new QuestionTirage();
                
                // On lui attribue une question
                $questionTirage->setQuestion($question);
                // On définie le marquage de la question à "Non marquée" par défaut
                $questionTirage->setEstmarquee(0);
                // On lui attribue l'inscription courante
                $questionTirage->setInscription($inscription);
                
                // On garde en mémoire l'entité QuestionTirage
                $em->persist($questionTirage);
        
            }
        }
          
        // On enregistre les données
        $em->flush();
    }

    // Gestion du formulaire d'affichage des questions
    protected function getFormQuestion(Question $question)
    {
        // Indice permettant de connaitre le numéro de la question actuelle
 
            $reponses = $question->getReponses();
            
            $tabReponses = array();
            
            // On récupère l'id et l'énoncé des réponses
            foreach($reponses as $reponse){
                $tabReponses[$reponse->getId()] = $reponse->getEnonce();
            }
            
            // Création du formulaire
            $form = $this->createFormBuilder();
                  
            // Si le type de la question vaut 0, il s'agit d'une question à choix simple
            // champ 'multiple' à false pour n'avoir que des boutons radio
            if($question->getType() == 0)
            {
                $form->add('reponses','choice',array(
                            'expanded' => true,
                            'multiple' => false,
                            'choices' => $tabReponses,
                            'data' => array_keys($tabReponses)[0]
                ));
            }
            // Si le type de la question vaut 1, il s'agit d'une question à choix multiple
            // champ 'multipe' à true pour avoir plusieurs checkbox
            else if ($question->getType() == 1)
            {
                $form->add('reponses','choice',array(
                            'expanded' => true,
                            'multiple' => true,
                            'choices' => $tabReponses
                ));
            }
            
            // Passe à la question suivante
            $form->add('suivant', 'submit');
            // Renvoie vers la page récapitulative du test
            $form->add('recapitulatif', 'submit');

      
        // On construit le formulaire
        $questionForm = $form->getForm();
        
        return $questionForm;
    }
    
    // Permet d'obtenir toutes les questions tirées
    protected function getTableauQuestions(){
        
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('QcmBackBundle:QuestionTirage');
        $questionsTirage = $repository->findAll();
        
        return $questionsTirage;
    }
}
                                                         
