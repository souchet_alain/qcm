<?php

namespace Qcm\BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityRepository;

use Qcm\BackBundle\Form\QuestionType;
use Qcm\BackBundle\Entity\Question;

class QuestionsController extends Controller
{
    public function indexAction()
    {
        
        // Assignation des variables au template et génération de la page
        $template = 'QcmBackBundle:Questions:index.html.twig';
        $options = array(
            'page' => 'questions'
        );
        
        return $this->render($template, $options);
        
    }
    
    public function ajouterAction(Request $request)
    {
      
        // Création du formulaire de saisie de question
        $questionForm = $this->creerQuestionForm();
        $questionForm->handleRequest($request);        
        
        // Test de la validité de la soumission du formulaire
        if ($questionForm->isValid()) {
            
            $this->ajouterQuestion($questionForm);
            
            return $this->redirect($this->generateUrl('questions_ajouter'));
            
        } 
        
        // Assignation des variables au template et génération de la page
        $template = 'QcmBackBundle:Questions:ajouter.html.twig';
        $options = array(
            'page' => 'questions', 
            'form' => $questionForm->createView()
        );
        
        return $this->render($template, $options);
        
    }
    
    public function listeAction()
    {
        
        // Assignation des variables au template et génération de la page
        $template = 'QcmBackBundle:Questions:liste.html.twig';
        $options = array(
            'page' => 'questions'
        );
        
        return $this->render($template, $options);
        
    }
    
    public function statistiquesAction()
    {
        
        // Assignation des variables au template et génération de la page
        $template = 'QcmBackBundle:Questions:statistiques.html.twig';
        $options = array(
            'page' => 'questions'
        );
        
        return $this->render($template, $options);
        
    }
    
    private function creerQuestionForm() {
        
        // Création du formulaire
        $form = $this->createForm(new QuestionType);
        
        // Ajout de boutons de validation
        $validerOptions = array(
            'label' => 'Valider', 
            'attr' => array('value' => 'Valider')
        );
        $form->add('valider', 'submit', $validerOptions);
        
        // Renvoi du formulaire
        return $form;
        
    }
    
    private function ajouterQuestion($form) {
        
        // Création d'un Entity Manager
        $em = $this->getDoctrine()->getManager();
        
        // Création de l'objet question saisi et requête d'insertion
        $question = $form->getData();
        
        // Récupération des réponses
        $reponses = $form->get('reponses')->getData();
        
        // Initialisation du nombre de réponses correctes identifiées
        $nbReponsesCorrectes = 0;
        foreach ($reponses as $reponse) {
            
            // Association de la question à la réponse traitée
            $reponse->setQuestion($question);
            
            // Si la réponse évaluée est cochée comme bonne, incrémentation du compteur
            if ($reponse->getEstBonne() === 0) {
                $nbReponsesCorrectes++;
            }
            
        }
       
        $question->setReponses($reponses);
        
        $em->persist($question);
        
        // Commit des requête éffectuées
        $em->flush();
        
    }
    
}
