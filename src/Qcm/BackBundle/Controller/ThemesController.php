<?php

namespace Qcm\BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Qcm\BackBundle\Form\ThemeType;

class ThemesController extends Controller
{
    public function indexAction()
    {
        
        // Assignation des variables au template et affichage de la page
        $template = 'QcmBackBundle:Themes:index.html.twig';
        $options = array('page'=>'themes');
        
        return $this->render($template, $options);
        
    }
    
    public function ajouterAction(Request $request)
    {
        
        // Création du formulaire de saisie de thème
        $themeForm = $this->creerThemeForm();
        
        // Test de validation de la saisie du formulaire
        $themeForm->handleRequest($request);
        
        if ($themeForm->isValid()) {
            
            $this->ajouterTheme($themeForm);
            
            //Redirection vers la page de saisie de thèmes
            return $this->redirect($this->generateUrl('themes_ajouter'));
            
        }
        
        // Assignation des variables au template et affichage de la page
        $template = 'QcmBackBundle:Themes:ajouter.html.twig';
        $options = array(
            'page' => 'themes', 
            'form' => $themeForm->createView()
        );
        
        return $this->render($template, $options);
        
    }
    
    private function creerThemeForm() {
        
        $form = $this->createForm(new ThemeType);
        
        // Ajout d'un bouton de soumissions de formulaire
        $validerOptions = array('label' => 'Valider');
        $form->add('valider', 'submit', $validerOptions);
        
        // Renvoi du formulaire
        return $form;
        
    }

    private function ajouterTheme($form) {
        
        // Récupération du thème passé au formulaire
        $theme = $form->getData();
        
        // Insertion du formulaire en base
        $em = $this->getDoctrine()->getManager();
        $em->persist($theme);
        $em->flush();
        
    }
    
    
}
