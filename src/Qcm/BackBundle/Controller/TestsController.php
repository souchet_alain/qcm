<?php

namespace Qcm\BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Qcm\BackBundle\Form\TestType;

class TestsController extends Controller
{
    public function indexAction()
    {
        
        // Assignation des variables au template et affichage de la page
        $template = 'QcmBackBundle:Tests:index.html.twig';
        $options = array('page'=>'tests');
        
        return $this->render($template, $options);

    }
    
    public function ajouterAction(Request $request)
    {
        
        // Création du formulaire de test
        $testForm = $this->creerTestForm();
        $testForm->handleRequest($request);
        
        // Test de validité de la soumission du formulaire
        if ($testForm->isValid()) {
            
            // Ajout du test en base
            $this->ajouterTest($testForm);
            
            // Redirection sur la page de saisie de test
            return $this->redirect($this->generateUrl('tests_ajouter'));
            
        }
        
        // Assignation des variables au template et affichage de la page
        $template = 'QcmBackBundle:Tests:ajouter.html.twig';
        $options = array(
            'page' => 'tests', 
            'form' => $testForm->createView()
        );
        
        return $this->render($template, $options);

    }
    
    private function creerTestForm() {
        
        // Création du formulaire de saisie de test
        $form = $this->createForm(new TestType());
        
        // Ajout de boutons de validation
        $validerOptions = array(
            'label' => 'Valider', 
            'attr' => array('value' => 'Valider')
        );
        $form->add('valider', 'submit', $validerOptions);
        
        // Renvoi du formulaire
        return $form;
        
    }
    
    private function ajouterTest($form) {
        
        // Création de 'Entity manager
        $em = $this->getDoctrine()->getManager();
        
        // Récupération de l'objet Test saisi
        $test = $form->getData();
        
        // Récupération des sections de test saisies et association avec le test courant
        $sections = $form->get('sections')->getData();
        
        foreach ($sections as $section) {
            
            $section->setTest($test);
            
        }
        
        $test->setSections($sections);
        
        // Insertion en base du test
        $em->persist($test);
        $em->flush();
        
    }
    
    
    
}
