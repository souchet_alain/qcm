<?php

namespace Qcm\BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller
{
    public function indexAction()
    {
    
        // Assignation de données au template et génération de la page
        $template = 'QcmBackBundle:Admin:index.html.twig';
        $options = array('page'=>'accueil');
        
        return $this->render($template, $options);
        
    }

}
