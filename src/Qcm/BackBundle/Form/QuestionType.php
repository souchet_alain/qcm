<?php

namespace Qcm\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\File;


use Qcm\BackBundle\Form\ReponseProposeeType;
use Qcm\BackBundle\Entity\Theme;
use Qcm\BackBundle\Validator\Constraints\TwoAnswers;
use Qcm\BackBundle\Validator\Constraints\UniqueAnswer;

class QuestionType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // Définition des options des éléments du formulaire
        //*** Enonce de la question
        $enonceContraintes = array(
            new NotBlank, 
            new Length(array(
                'min' => 5, 
                'max' => 255, 
                'minMessage' => "L'énoncé d'une question doit comporter au moins 5 caractères.",
                'maxMessage' => "L'énoncé d'une question doit comporter moins de 255 caractères."
            ))
        );
        $enonceOptions = array(
            'label' => 'Enoncé : ', 
            'constraints' => $enonceContraintes
        );
        
        //*** Fichier joint à la question
        $mediaContraintes = array(
            new File
        );
        $mediaOptions = array(
            'label' => 'Attachement : ', 
            'required' => false, 
            'constraints' => $mediaContraintes
        );
        
        //*** Type de question
        $typeOptions = array(
            'label' => 'Type : ', 
            'expanded' => true, 
            'multiple' => false, 
            'choices' => array(
                0 => 'Simple', 
                1 => 'Multiple'
            ), 
            'data' => 0
        );
        
        //*** thème associé à la question
        $themeOptions = array(
            'label' => 'Thème : ', 
            'expanded' => false, 
            'multiple' => false, 
            'class' => 'QcmBackBundle:Theme', 
            'query_builder' => function (EntityRepository $repository) {
                
                return $repository->createQueryBuilder('theme')
                        ->orderBy('theme.libelle', 'ASC');
            
            }
        );
        
        //*** Réponses associées à la question
        $reponsesContraintes = array(
            new TwoAnswers
        );
        $reponsesOptions = array(
            'label' => 'Réponses : ', 
            'type' => new ReponseProposeeType(), 
            'allow_add' => true, 
            'by_reference' => false, 
            'constraints' => $reponsesContraintes
        );
        
        $builder
            ->add('enonce', 'textarea', $enonceOptions)
            ->add('media', 'file', $mediaOptions)
            ->add('type', 'choice', $typeOptions)
            ->add('theme', 'entity', $themeOptions)
            ->add('reponses', 'collection', $reponsesOptions)
        ;
        
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Qcm\BackBundle\Entity\Question'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'question';
    }
}
