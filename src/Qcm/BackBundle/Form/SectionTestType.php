<?php

namespace Qcm\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Range;
use Doctrine\ORM\EntityRepository;


class SectionTestType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // Définition des options des éléments du formulaire
        //*** Thème associé à la section
        $themeContraintes = array();
        $themeOptions = array(
            'label' => 'Thème : ', 
            'constraints' => $themeContraintes, 
            'class' => 'QcmBackBundle:Theme', 
            'query_builder' => function (EntityRepository $repository) {
                
                    return $repository->createQueryBuilder('theme')
                        ->orderBy('theme.libelle', 'ASC');
            
                }
        );
        
        //*** Nombre de questions à sélectionner
        $nbQuestionsContraintes = array(
            new Range(array(
                'min' => 1, 
                'minMessage' => 'Une section doit comporter au moins une question.'
            ))
        );
        $nbQuestionsOptions = array(
            'label' => 'Nombre de questions : ', 
            'constraints' => $nbQuestionsContraintes
        );
        
        // Builder du formulaire
        $builder
            ->add('theme', 'entity', $themeOptions)
            ->add('nbquestions', 'number', $nbQuestionsOptions)
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Qcm\BackBundle\Entity\SectionTest'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sectionTest';
    }
}
