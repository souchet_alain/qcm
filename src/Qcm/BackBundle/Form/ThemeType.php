<?php

namespace Qcm\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;

class ThemeType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // Contraintes de validation
        $enonceContraintes = array(
            new NotBlank, 
            new Length(array(
                'min' => 3, 
                'max' => 255, 
                'minMessage' => 'L\'intitulé du thème doit comporter au moins 3 caractères.', 
                'maxMessage' => 'L\'intitulé du thème doit comporter moins de 255 caractères.'
            ))
        );
        // Options de l'énoncé
        $enonceOptions = array(
            'label' => 'Libellé : ', 
            'constraints' => $enonceContraintes
        );
        
        $builder
            ->add('libelle', 'textarea', $enonceOptions)
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Qcm\BackBundle\Entity\Theme'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'qcm_backbundle_theme';
    }
}
