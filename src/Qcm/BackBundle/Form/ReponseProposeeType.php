<?php

namespace Qcm\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;


class ReponseProposeeType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // Définition des options et contraintes des éléments du formulaire
        //*** Libellé de la réponse
        $libelleContraintes = array(
            new NotBlank, 
            new Length(array(
                'max' => 512, 
                'maxMessage' => "L'énoncé d'une réponse doit comporter moins de 512 caractères."
            ))
        );
        $libelleOptions = array(
            'label' => 'Libellé : ', 
            'constraints' => $libelleContraintes
        );
        
        //*** Checkbox de réponse correcte
        $reponseCorrecteOptions = array(
            'label' => 'Réponse correcte : ', 
            'required' => false
        );
        
        $builder
            ->add('enonce', 'textarea', $libelleOptions)
            ->add('estbonne', 'checkbox', $reponseCorrecteOptions)
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Qcm\BackBundle\Entity\ReponseProposee'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'reponseProposee';
    }
}
