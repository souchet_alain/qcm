<?php

namespace Qcm\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Range;
use Doctrine\ORM\EntityRepository;


class TestType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // Définition des options et contraintes sur les champs du formulaire
        //*** Libellé du test
        $libelleContraintes = array(
            new Length(array(
                'min' => 3, 
                'max' => 128, 
                'minMessage' => 'Le titre du test doit comporter au moins 3 caractères.', 
                'maxMessage' => 'Le titre du test doit comporter moins de 128 caractères'
            ))
        );
        $libelleOptions = array(
            'label' => 'Titre : ', 
            'constraints' => $libelleContraintes
        );
        
        //*** Description du test
        $descriptionContraintes = array(
            new Length(array(
                'min' => 5, 
                'max' => 512, 
                'minMessage' => 'Le titre du test doit comporter au moins 5 caractères.', 
                'maxMessage' => 'Le titre du test doit comporter moins de 512 caractères'
            ))
        );
        $descriptionOptions = array(
            'label' => 'Description : ', 
            'constraints' => $descriptionContraintes
        );
        
        //*** Auteur du test
        $auteurContraintes = array();
        $auteurOptions = array(
            'label' => 'Auteur : ', 
            'constraints' => $auteurContraintes, 
            'class' => 'QcmBackBundle:Utilisateur', 
            'query_builder' => function (EntityRepository $repository) {
                
                return $repository->createQueryBuilder('user')
                        ->orderBy('user.nom', 'ASC', 'user.prenom', 'ASC');
            }
            
        );
        
        //*** Durée du test
        $dureeContraintes = array(
            new Range(array(
                'min' => 1, 
                'max' => 1440,
                'minMessage' => 'La durée du test doit être supérieure à 1min.', 
                'maxMessage' => 'La duréee du test doit être inférieure à 1440min.'
            ))
        );
        $dureeOptions = array(
            'label' => 'Durée du test : ', 
            'constraints' => $dureeContraintes
        );
        
        //*** Seuil de réussite du test
        $seuilContraintes = array(
            new Range(array(
                'min' => 0,
                'max' => 100, 
                'minMessage' => 'Le seuil de réussite doit être supérieur à 0%.', 
                'maxMessage' => 'Le seuil de réussite doit être inférieur à 100%.'
            ))
        );
        $seuilOptions = array(
            'label' => 'Seuil de réussite : ', 
            'constraints' => $seuilContraintes
        );
        
        //*** Sections associées au test
        $sectionsContraintes = array();
        $sectionsOptions = array(
            'label' => 'Sections du test : ', 
            'type' => new SectionTestType(), 
            'allow_add' => true, 
            'by_reference' => false, 
            'constraints' => $sectionsContraintes
        );
        
        $builder
            ->add('libelle', 'text', $libelleOptions)
            ->add('description', 'textarea', $descriptionOptions)
            ->add('creer', 'entity', $auteurOptions)
            ->add('duree', 'number', $dureeOptions)
            ->add('seuil', 'number', $seuilOptions)
            ->add('sections', 'collection', $sectionsOptions)
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Qcm\BackBundle\Entity\Test'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'test';
    }
}
