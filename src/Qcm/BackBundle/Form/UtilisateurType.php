<?php

namespace Qcm\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Form\Type\RegistrationFormType;

use Qcm\BackBundle\Entity\Profil;

class UtilisateurType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $nomOptions = array(
            'label' => 'Nom : '
        );
        $prenomOptions = array(
            'label' => 'Prénom : '
        );
        $datenaissanceOptions = array(
            'label' => 'Date de naissance : '
        );
        $adresseOptions = array(
            'label' => 'Adresse : '
        );
        $codepostalOptions = array(
            'label' => 'Code postal : '
        );
        $villeOptions = array(
            'label' => 'Ville : '
        );
        $codepromotionOptions = array(
            'label' => 'Code promotion : ', 
            'expanded' => false, 
            'multiple' => false, 
            'class' => 'QcmBackBundle:Promotion', 
            'query_builder' => function (EntityRepository $repository) {
                
                return $repository->createQueryBuilder('p')
                        ->orderBy('p.libelle', 'ASC');
            
            }
        );
        $profilOptions = array(
            'label' => '', 
            'type' => new RegistrationFormType('Qcm\BackBundle\Entity\Profil'), 
            'allow_add' => true, 
            'by_reference' => false
        );
        
        $builder
            ->add('nom', 'text', $nomOptions)
            ->add('prenom', 'text', $prenomOptions)
            ->add('datenaissance', 'date', $datenaissanceOptions)
            ->add('adresse', 'text', $adresseOptions)
            ->add('codepostal', 'integer', $codepostalOptions)
            ->add('ville', 'text', $villeOptions)
            ->add('codepromotion', 'entity', $codepromotionOptions)
            ->add('profil', 'collection', $profilOptions)
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Qcm\BackBundle\Entity\Utilisateur'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'utilisateur';
    }
}
