<?php

namespace Qcm\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReponseDonnee
 *
 * @ORM\Table(name="reponse_donnee")
 * @ORM\Entity
 */
class ReponseDonnee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var \ReponseProposee
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="ReponseProposee")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="reponse_proposee", referencedColumnName="id")
     * })
     */
    private $reponseProposee;

    /**
     * @var \QuestionTirage
     *
     * @ORM\ManyToOne(targetEntity="QuestionTirage", inversedBy="reponsesDonnees")
     * @ORM\JoinColumn(name="question", referencedColumnName="id")
     */
    private $question;

    /**
     * @var \Inscription
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Inscription")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inscription", referencedColumnName="id")
     * })
     */
    private $inscription;



    /**
     * Set id
     *
     * @param integer $id
     * @return ReponseDonnee
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set inscription
     *
     * @param \Qcm\BackBundle\Entity\Inscription $inscription
     * @return ReponseDonnee
     */
    public function setInscription(\Qcm\BackBundle\Entity\Inscription $inscription = null)
    {
        $this->inscription = $inscription;
    
        return $this;
    }

    /**
     * Get inscription
     *
     * @return \Qcm\BackBundle\Entity\Inscription 
     */
    public function getInscription()
    {
        return $this->inscription;
    }

    /**
     * Set question
     *
     * @param \Qcm\BackBundle\Entity\Question $question
     * @return ReponseDonnee
     */
    public function setQuestion(\Qcm\BackBundle\Entity\Question $question = null)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get question
     *
     * @return \Qcm\BackBundle\Entity\Question 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set reponseProposee
     *
     * @param \Qcm\BackBundle\Entity\ReponseProposee $reponseProposee
     * @return ReponseDonnee
     */
    public function setReponseProposee(\Qcm\BackBundle\Entity\ReponseProposee $reponseProposee = null)
    {
        $this->reponseProposee = $reponseProposee;
    
        return $this;
    }

    /**
     * Get reponseProposee
     *
     * @return \Qcm\BackBundle\Entity\ReponseProposee 
     */
    public function getReponseProposee()
    {
        return $this->reponseProposee;
    }
}