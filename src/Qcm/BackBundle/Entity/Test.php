<?php

namespace Qcm\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Test
 *
 * @ORM\Table(name="test")
 * @ORM\Entity
 */
class Test
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=128, nullable=false)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=512, nullable=false)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="duree", type="float", nullable=false)
     */
    private $duree;

    /**
     * @var integer
     *
     * @ORM\Column(name="seuil", type="integer", nullable=false)
     */
    private $seuil;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="creer", referencedColumnName="id")
     * })
     */
    private $creer;
    
    /**
     * @var \SectionTest
     * 
     * @ORM\OneToMany(targetEntity="SectionTest", mappedBy="test", cascade={"all"}) 
     */
    private $sections;


    public function __construct() {
        
        $this->sections = new ArrayCollection();
        
    }
    
    

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Test
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    
        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Test
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set duree
     *
     * @param float $duree
     * @return Test
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;
    
        return $this;
    }

    /**
     * Get duree
     *
     * @return float 
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Set seuil
     *
     * @param integer $seuil
     * @return Test
     */
    public function setSeuil($seuil)
    {
        $this->seuil = $seuil;
    
        return $this;
    }

    /**
     * Get seuil
     *
     * @return integer 
     */
    public function getSeuil()
    {
        return $this->seuil;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set creer
     *
     * @param \Qcm\BackBundle\Entity\Utilisateur $creer
     * @return Test
     */
    public function setCreer(\Qcm\BackBundle\Entity\Utilisateur $creer = null)
    {
        $this->creer = $creer;
    
        return $this;
    }

    /**
     * Get creer
     *
     * @return \Qcm\BackBundle\Entity\Utilisateur 
     */
    public function getCreer()
    {
        return $this->creer;
    }

    public function getSections() {
        return $this->sections;
    }

    public function setSections(SectionTest $sections) {
        
        $this->sections = $sections;
        return $this;
        
    }

    public function addSection(SectionTest $section) {
        
        $this->sections[] = $section;
        return $this;
        
    }
    
    public function removeSection(SectionTest $section) {
        
        $this->sections->remove($section);
        return $this;
        
    }

}