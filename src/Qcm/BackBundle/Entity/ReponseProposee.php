<?php

namespace Qcm\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReponseProposee
 *
 * @ORM\Table(name="reponse_proposee")
 * @ORM\Entity
 */
class ReponseProposee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="enonce", type="string", length=512, nullable=false)
     */
    private $enonce;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estBonne", type="boolean", nullable=false)
     */
    private $estbonne;

    /**
     * @var \Question
     *
     * @ORM\ManyToOne(targetEntity="Question", inversedBy="reponses")
     * @ORM\JoinColumn(name="question", referencedColumnName="id")
     */
    private $question;



    /**
     * Set enonce
     *
     * @param string $enonce
     * @return ReponseProposee
     */
    public function setEnonce($enonce)
    {
        $this->enonce = $enonce;
    
        return $this;
    }

    /**
     * Get enonce
     *
     * @return string 
     */
    public function getEnonce()
    {
        return $this->enonce;
    }

    /**
     * Set estbonne
     *
     * @param boolean $estbonne
     * @return ReponseProposee
     */
    public function setEstbonne($estbonne)
    {
        $this->estbonne = $estbonne;
    
        return $this;
    }

    /**
     * Get estbonne
     *
     * @return boolean 
     */
    public function getEstbonne()
    {
        return $this->estbonne;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param \Qcm\BackBundle\Entity\Question $question
     * @return ReponseProposee
     */
    public function setQuestion(\Qcm\BackBundle\Entity\Question $question = null)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get question
     *
     * @return \Qcm\BackBundle\Entity\Question 
     */
    public function getQuestion()
    {
        return $this->question;
    }
}