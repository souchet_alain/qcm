<?php

namespace Qcm\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * SectionTest
 *
 * @ORM\Table(name="section_test")
 * @ORM\Entity
 */
class SectionTest
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbQuestions", type="integer", nullable=false)
     */
    private $nbquestions;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Test", inversedBy="sections")
     * @ORM\JoinColumn(name="test", referencedColumnName="id")
     * })
     */
    private $test;

    /**
     * @var \Theme
     *
     * @ORM\ManyToOne(targetEntity="Theme", inversedBy="sections")
     * @ORM\JoinColumn(name="theme", referencedColumnName="id")
     */
    private $theme;


    public function __construct() {
        $this->test = new ArrayCollection();
    }
    
    /**
     * Set nbquestions
     *
     * @param integer $nbquestions
     * @return SectionTest
     */
    public function setNbquestions($nbquestions)
    {
        $this->nbquestions = $nbquestions;
    
        return $this;
    }

    /**
     * Get nbquestions
     *
     * @return integer 
     */
    public function getNbquestions()
    {
        return $this->nbquestions;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set theme
     *
     * @param \Qcm\BackBundle\Entity\Theme $theme
     * @return SectionTest
     */
    public function setTheme(\Qcm\BackBundle\Entity\Theme $theme = null)
    {
        $this->theme = $theme;
    
        return $this;
    }

    /**
     * Get theme
     *
     * @return \Qcm\BackBundle\Entity\Theme 
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set test
     *
     * @param \Qcm\BackBundle\Entity\Test $test
     * @return SectionTest
     */
    public function setTest(\Qcm\BackBundle\Entity\Test $test = null)
    {
        $this->test = $test;
    
        return $this;
    }

    /**
     * Get test
     *
     * @return \Qcm\BackBundle\Entity\Test 
     */
    public function getTest()
    {
        return $this->test;
    }
    
    public function addTest($test)
    {
        $this->test->add($test);
    }
}