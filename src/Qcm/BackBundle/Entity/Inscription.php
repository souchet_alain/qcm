<?php

namespace Qcm\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Inscription
 *
 * @ORM\Table(name="inscription")
 * @ORM\Entity(repositoryClass="Qcm\BackBundle\Entity\InscriptionRepository")
 */
class Inscription
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="dureeValidite", type="float", nullable=false)
     */
    private $dureevalidite;

    /**
     * @var float
     *
     * @ORM\Column(name="tempsEcoule", type="float", nullable=false)
     */
    private $tempsecoule;

    /**
     * @var string
     *
     * @ORM\Column(name="etat", type="string", length=12, nullable=false)
     */
    private $etat;

    /**
     * @var float
     *
     * @ORM\Column(name="resultat", type="float", nullable=true)
     */
    private $resultat;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="candidat", referencedColumnName="id")
     * })
     */
    private $candidat;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="creerPar", referencedColumnName="id")
     * })
     */
    private $creerpar;

    /**
     * @var \Test
     *
     * @ORM\ManyToOne(targetEntity="Test")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="test", referencedColumnName="id")
     * })
     */
    private $test;



    /**
     * Set dureevalidite
     *
     * @param float $dureevalidite
     * @return Inscription
     */
    public function setDureevalidite($dureevalidite)
    {
        $this->dureevalidite = $dureevalidite;
    
        return $this;
    }

    /**
     * Get dureevalidite
     *
     * @return float 
     */
    public function getDureevalidite()
    {
        return $this->dureevalidite;
    }

    /**
     * Set tempsecoule
     *
     * @param float $tempsecoule
     * @return Inscription
     */
    public function setTempsecoule($tempsecoule)
    {
        $this->tempsecoule = $tempsecoule;
    
        return $this;
    }

    /**
     * Get tempsecoule
     *
     * @return float 
     */
    public function getTempsecoule()
    {
        return $this->tempsecoule;
    }

    /**
     * Set etat
     *
     * @param string $etat
     * @return Inscription
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    
        return $this;
    }

    /**
     * Get etat
     *
     * @return string 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set resultat
     *
     * @param float $resultat
     * @return Inscription
     */
    public function setResultat($resultat)
    {
        $this->resultat = $resultat;
    
        return $this;
    }

    /**
     * Get resultat
     *
     * @return float 
     */
    public function getResultat()
    {
        return $this->resultat;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set test
     *
     * @param \Qcm\BackBundle\Entity\Test $test
     * @return Inscription
     */
    public function setTest(\Qcm\BackBundle\Entity\Test $test = null)
    {
        $this->test = $test;
    
        return $this;
    }

    /**
     * Get test
     *
     * @return \Qcm\BackBundle\Entity\Test 
     */
    public function getTest()
    {
        return $this->test;
    }

    /**
     * Set creerpar
     *
     * @param \Qcm\BackBundle\Entity\Utilisateur $creerpar
     * @return Inscription
     */
    public function setCreerpar(\Qcm\BackBundle\Entity\Utilisateur $creerpar = null)
    {
        $this->creerpar = $creerpar;
    
        return $this;
    }

    /**
     * Get creerpar
     *
     * @return \Qcm\BackBundle\Entity\Utilisateur 
     */
    public function getCreerpar()
    {
        return $this->creerpar;
    }

    /**
     * Set candidat
     *
     * @param \Qcm\BackBundle\Entity\Utilisateur $candidat
     * @return Inscription
     */
    public function setCandidat(\Qcm\BackBundle\Entity\Utilisateur $candidat = null)
    {
        $this->candidat = $candidat;
    
        return $this;
    }

    /**
     * Get candidat
     *
     * @return \Qcm\BackBundle\Entity\Utilisateur 
     */
    public function getCandidat()
    {
        return $this->candidat;
    }
}