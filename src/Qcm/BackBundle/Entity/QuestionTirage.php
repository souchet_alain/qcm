<?php

namespace Qcm\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * QuestionTirage
 *
 * @ORM\Table(name="question_tirage")
 * @ORM\Entity
 */
class QuestionTirage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estMarquee", type="boolean", nullable=false)
     */
    private $estmarquee;

    /**
     * @var \Inscription
     *
     * @ORM\ManyToOne(targetEntity="Inscription")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inscription", referencedColumnName="id")
     * })
     */
    private $inscription;

    /**
     * @var \Question
     *
     * @ORM\ManyToOne(targetEntity="Question", inversedBy="questionsTirage")
     * @ORM\JoinColumn(name="question", referencedColumnName="id")
     */
    private $question;
    
    /**
     * @var \ReponseDonnee
     * 
     * @ORM\OneToMany(targetEntity="ReponseDonnee", mappedBy="question", cascade={"all"}) 
     */
    private $reponsesDonnees;


    public function __construct() {
     
        $this->reponsesDonnees = new ArrayCollection();
        
    }

    
    
    /**
     * Set estmarquee
     *
     * @param boolean $estmarquee
     * @return QuestionTirage
     */
    public function setEstmarquee($estmarquee)
    {
        $this->estmarquee = $estmarquee;
    
        return $this;
    }

    /**
     * Get estmarquee
     *
     * @return boolean 
     */
    public function getEstmarquee()
    {
        return $this->estmarquee;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param \Qcm\BackBundle\Entity\Question $question
     * @return QuestionTirage
     */
    public function setQuestion(\Qcm\BackBundle\Entity\Question $question = null)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get question
     *
     * @return \Qcm\BackBundle\Entity\Question 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set inscription
     *
     * @param \Qcm\BackBundle\Entity\Inscription $inscription
     * @return QuestionTirage
     */
    public function setInscription(\Qcm\BackBundle\Entity\Inscription $inscription = null)
    {
        $this->inscription = $inscription;
    
        return $this;
    }

    /**
     * Get inscription
     *
     * @return \Qcm\BackBundle\Entity\Inscription 
     */
    public function getInscription()
    {
        return $this->inscription;
    }
    
    public function getReponsesDonnees() {
        return $this->reponsesDonnees;
    }

    public function setReponsesDonnees($reponsesDonnees) {
        $this->reponsesDonnees = $reponsesDonnees;
        return $this;
    }

    public function addReponseDonnee(ReponseProposee $reponse) {
        $this->reponsesDonnees[] = $reponse;
        return $this;
    }
    
    public function removeReponseDonnee(ReponseProposee $reponse) {
        $this->reponsesDonnees->remove($reponse);
        return $this;
    }
    
    
    
    
}