<?php

namespace Qcm\BackBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Qcm\BackBundle\Entity\Utilisateur;

/**
 * Description of InscriptionRepository
 */
class InscriptionRepository extends EntityRepository {
    
    /**
     * Permet de récupérer les informations d'inscription d'un candidat
     */
    public function getInscriptionsCandidat(Utilisateur $candidat) {
        
        $request = 'SELECT i FROM QcmBackBundle:Inscription i WHERE i.candidat=:candidat';
         
        $query = $this->getEntityManager()->createQuery($request);
        $query->setParameter('candidat', $candidat);
        
        return $query->getResult();
        
    }
    
    /**
     * Permet de récupérer le nombre de questions
     */
    public function getSections(){
        $request = 'SELECT s FROM QcmBackBundle:SectionTest s JOIN s.test i';
        
        $query = $this->getEntityManager()->createQuery($request);
        
        return $query->getResult();
    }
}
