<?php

namespace Qcm\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Qcm\BackBundle\Entity\Theme;

/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity
 */
class Question
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="enonce", type="string", length=255, nullable=false)
     */
    private $enonce;

    /**
     * @var string
     *
     * @ORM\Column(name="media", type="string", length=64, nullable=true)
     */
    private $media;

    /**
     * @var boolean
     *
     * @ORM\Column(name="type", type="boolean", nullable=false)
     */
    private $type;

    /**
     * @var \Theme
     *
     * @ORM\ManyToOne(targetEntity="Theme", inversedBy="questions")
     * @ORM\JoinColumn(name="theme", referencedColumnName="id")
     * })
     */
    private $theme;
    
    /**
     * @var \ReponseProposee
     *
     * @ORM\OneToMany(targetEntity="ReponseProposee", mappedBy="question", cascade={"all"})
     */
    private $reponses;
    
    /**
     * @var \QuestionTirage
     * 
     * @ORM\OneToMany(targetEntity="QuestionTirage", mappedBy="question", cascade={"all"})
     */
    private $questionsTirage;


    public function __construct() {
        
        $this->reponses = new ArrayCollection();
        $this->questionsTirage = new ArrayCollection();
        
    }

    /**
     * Set enonce
     *
     * @param string $enonce
     * @return Question
     */
    public function setEnonce($enonce)
    {
        $this->enonce = $enonce;
    
        return $this;
    }

    /**
     * Get enonce
     *
     * @return string 
     */
    public function getEnonce()
    {
        return $this->enonce;
    }

    /**
     * Set media
     *
     * @param string $media
     * @return Question
     */
    public function setMedia($media)
    {
        $this->media = $media;
    
        return $this;
    }

    /**
     * Get media
     *
     * @return string 
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set type
     *
     * @param boolean $type
     * @return Question
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return boolean 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set theme
     *
     * @param \Qcm\BackBundle\Entity\Theme $theme
     * @return Question
     */
    public function setTheme($theme = null)
    {
        $this->theme = $theme;
    
        return $this;
    }

    public function setReponses($reponses) {
        
        foreach ($reponses as $reponse) {
            $this->addReponse($reponse);
        }
        
        
        $this->reponses = $reponses;
        return $this;
        
    }
    
    public function getReponses() {
        return $this->reponses;
    }

        
    
    public function addReponse(ReponseProposee $reponse) {
        
        $this->reponses[] = $reponse;
        return $this;
        
    }
    
    public function removeReponse(ReponseProposee $reponse) {
        
        $this->reponses->remove($reponse);
        return $this;
        
    }
    
    public function getQuestionsTirage() {
        return $this->questionsTirage;
    }

    public function setQuestionsTirage($questionsTirage) {
        $this->questionsTirage = $questionsTirage;
        return $this;
    }

    public function addQuestionTirage(QuestionTirage $question) {
        $this->questionsTirage[] = $question;
        return $this;
    }
    
    public function removeQuestionTirage(QuestionTirage $question) {
        $this->questionsTirage->remove($question);
        return $this;
    }
    
        
    /**
     * Get theme
     *
     * @return \Qcm\BackBundle\Entity\Theme 
     */
    public function getTheme()
    {
        return $this->theme;
    }
    
    public function getNbBonnesReponses() {
        
        // Initialisation du nombre de réponse correctes
        $nbReponsesBonnes = 0;
        
        // Parcours des réponses
        foreach ($reponses as $reponse) {
            
            // Incrémentation du nombre de réponses correctes
            if ($reponse->getEstBonne() == 1) {
                $nbReponsesBonnes++;
            }
            
        }
        
        return $nbReponsesBonnes;
        
    }
}