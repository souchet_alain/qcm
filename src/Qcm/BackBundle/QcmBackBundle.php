<?php

namespace Qcm\BackBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class QcmBackBundle extends Bundle
{
    public function getParent() {
        
        return 'FOSUserBundle';
        
    }
    
}
