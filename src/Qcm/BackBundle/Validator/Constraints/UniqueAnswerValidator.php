<?php

namespace Qcm\BackBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

use Qcm\BackBundle\Entity\Question;

/**
 * Description of UniqueAnswerValidator
 *
 * @author Alain
 */
class UniqueAnswerValidator extends ConstraintValidator {
    
    public function validate($question, Constraint $contrainte) {
                
        if ($question->getType() == 0 && $question.getNbBonnesReponses() != 1) {
        
            $this->context->addViolation($contrainte->message);
        
        }
        
    }
   
}
