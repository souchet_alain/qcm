<?php

namespace Qcm\BackBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

use Qcm\BackBundle\Entity\Question;

/**
 * Description of TwoAnswersValidator
 *
 * @author asouchet2015
 */
class TwoAnswersValidator extends ConstraintValidator {
    
    public function validate($question, Constraint $contrainte) {
                
        var_dump($question);
        
        if (sizeof($question) < 2) {
        
            $this->context->addViolation($contrainte->message);
        
        }
        
    }
        
}
