<?php

namespace Qcm\BackBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class TwoAnswers extends Constraint {
    
    public $message = "La question doit posséder au moins deux réponses.";
    
}
