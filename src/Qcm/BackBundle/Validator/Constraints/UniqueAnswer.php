<?php

namespace Qcm\BackBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueAnswer extends Constraint {
    
    public $message = "La question doit posséder une et une seule réponse.";
    
}
