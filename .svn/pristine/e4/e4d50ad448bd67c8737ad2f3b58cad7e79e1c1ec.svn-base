<?php

namespace Qcm\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Qcm\FrontBundle\Entity\ReponseProposee;

/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity
 */
class Question
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="enonce", type="string", length=255, nullable=false)
     */
    private $enonce;

    /**
     * @var string
     *
     * @ORM\Column(name="media", type="string", length=64, nullable=true)
     */
    private $media;

    /**
     * @var boolean
     *
     * @ORM\Column(name="type", type="boolean", nullable=false)
     */
    private $type;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Inscription", mappedBy="question")
     */
    private $inscription;

    /**
     * @var \Theme
     *
     * @ORM\ManyToOne(targetEntity="Theme")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="theme", referencedColumnName="id", onDelete="Cascade")
     * })
     */
    private $theme;
    
    /**
     * @var \Question
     *
     * @ORM\OneToMany(targetEntity="ReponseProposee", mappedBy="question")
     */
    protected $reponses;
    
    

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->inscription = new ArrayCollection();
        $this->reponses = new ArrayCollection();
    }
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enonce
     *
     * @param string $enonce
     * @return Question
     */
    public function setEnonce($enonce)
    {
        $this->enonce = $enonce;
    
        return $this;
    }

    /**
     * Get enonce
     *
     * @return string 
     */
    public function getEnonce()
    {
        return $this->enonce;
    }

    /**
     * Set media
     *
     * @param string $media
     * @return Question
     */
    public function setMedia($media)
    {
        $this->media = $media;
    
        return $this;
    }

    /**
     * Get media
     *
     * @return string 
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set type
     *
     * @param boolean $type
     * @return Question
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return boolean 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add inscription
     *
     * @param \Qcm\FrontBundle\Entity\Inscription $inscription
     * @return Question
     */
    public function addInscription(\Qcm\FrontBundle\Entity\Inscription $inscription)
    {
        $this->inscription[] = $inscription;
    
        return $this;
    }

    /**
     * Remove inscription
     *
     * @param \Qcm\FrontBundle\Entity\Inscription $inscription
     */
    public function removeInscription(\Qcm\FrontBundle\Entity\Inscription $inscription)
    {
        $this->inscription->removeElement($inscription);
    }

    /**
     * Get inscription
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInscription()
    {
        return $this->inscription;
    }

    /**
     * Set theme
     *
     * @param \Qcm\FrontBundle\Entity\Theme $theme
     * @return Question
     */
    public function setTheme(\Qcm\FrontBundle\Entity\Theme $theme = null)
    {
        $this->theme = $theme;
    
        return $this;
    }

    /**
     * Get theme
     *
     * @return \Qcm\FrontBundle\Entity\Theme 
     */
    public function getTheme()
    {
        return $this->theme;
    }
    
    public function getReponses() {
        return $this->reponses;
    }

    public function setReponses(ArrayCollection $reponses) {
        $this->reponses = $reponses;
        return $this;
    }
    
    public function addReponse(ReponseProposee $reponse) {
        $this->reponses->add($reponse);
        return $this;
    }    

    public function __toString() {
        return $this->enonce;
    }

    
    
    
}