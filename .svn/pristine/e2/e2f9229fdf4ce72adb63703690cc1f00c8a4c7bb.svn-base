<?php

namespace Qcm\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Theme
 *
 * @ORM\Table(name="theme")
 * @ORM\Entity
 */
class Theme
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255, nullable=false)
     */
    private $libelle;

    /**
     * @var \SectionTest
     * 
     * @ORM\OneToMany(targetEntity="SectionTest", mappedBy="theme", cascade={"all"}) 
     */
    private $sections;


    private function __construct() {
        $this->section = new ArrayCollection();
    }
    

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Theme
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    
        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function __toString() {
        
        return $this->libelle;
        
    }

    public function getSections() {
        return $this->sections;
    }

    public function setSections(SectionTest $sections) {
        $this->sections = $sections;
        return $this;
    }
    
    public function addSection(SectionTest $section) {
        
        $this->sections[] = $section;
        return $this;
        
    }

    public function removeSection(SectionTest $section) {
        $this->sections->remove($section);
    }

}