$(document).ready(function () {
    
    // Initialisation des id des sections
    var idSection = 1;
   
    // récupération de la division contenant les sections
    var divSections = $('#test_sections');
    divSections.css('border', 'solid 2px black');

    // Ajout d'un bouton d'ajout de sections
    var lienAjouter = $('<a href="#" id="btnAjoutReponse" class="btn btn-default">Ajouter une section</a>')
    lienAjouter.insertAfter(divSections);
    
    // Ajout du formulaire de saisie de sections au clic sur le bouton
    lienAjouter.click(function (event) {
       
        // Ajout d'un formulaire de section
        addSectionForm(divSections, idSection);
        idSection++;
        
        // Supression du # dans l'adresse
        event.preventDefault(); 
        
    });
    
    
    function addSectionForm(conteneur, idSection) {
        
        // Récupération du prototype du formulaire de section
        var prototypeForm = $(conteneur.attr('data-prototype')
                .replace(/__name__label__/g, 'Section n°' + idSection)
                .replace(/__name__/g, idSection));
         
        // ajout du lien de suppression
        var lienSupprimer = $('<a href="#" id="btnSupprimerReponse" class="btn btn-warning">Supprimer</a>')
        lienSupprimer.click(function (event) {
            
            prototypeForm.remove();
            
            // Supression du # dans l'adresse
            event.preventDefault();
            
        });
        
        prototypeForm.append(lienSupprimer);
        
        // Ajout du formulaire de section à la division Sections
        conteneur.append(prototypeForm);
       
    }

})
    