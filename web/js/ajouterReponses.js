$(document).ready(function () {
    
    // Initialisation des id des réponses
    var idReponse = 1;
   
    // récupération de la division contenant les réponses
    var divReponses = $('#question_reponses');
    divReponses.css('border', 'solid 2px black');

    //  Création d'un premier formulaire de réponse
    addReponseForm(divReponses, idReponse++);

    // Ajout d'un bouton d'ajout de réponse
    var lienAjouter = $('<a href="#" id="btnAjoutReponse" class="btn btn-default">Ajouter une réponse</a>')
    lienAjouter.insertAfter(divReponses);
    
    // Ajout du formulaire de saisie de réponse au clic sur le bouton
    lienAjouter.click(function (event) {
       
        // Ajout d'un formulaire de réponse
        addReponseForm(divReponses, idReponse);
        idReponse++;
        
        // Supression du # dans l'adresse
        event.preventDefault(); 
        
    });
    
    
    function addReponseForm(conteneur, idReponse) {
        
        // Récupération du prototype du formulaire de réponse
        var prototypeForm = $(conteneur.attr('data-prototype')
                .replace(/__name__label__/g, 'Réponse n°' + idReponse)
                .replace(/__name__/g, idReponse));
         
        // ajout du lien de suppression
        var lienSupprimer = $('<a href="#" id="btnSupprimerReponse" class="btn btn-warning">Supprimer</a>')
        lienSupprimer.click(function (event) {
            
            prototypeForm.remove();
            
            // Supression du # dans l'adresse
            event.preventDefault();
            
        });
        
        prototypeForm.append(lienSupprimer);
        
        // Ajout du formulaire de réponse à la division Réponses
        conteneur.append(prototypeForm);
       
    }

})
    