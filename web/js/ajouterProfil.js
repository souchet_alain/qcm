$(document).ready(function () {
    
    // récupération de la division contenant le formulaire de profil
    var divProfil = $('#utilisateur_profil');
    divProfil.css('border', 'solid 2px black');

    //  Création du formulaire de profil
    addProfilForm(divProfil);

    function addProfilForm(conteneur) {
        
        // Récupération du prototype du formulaire de profil
        var prototypeForm = $(conteneur.attr('data-prototype')
                .replace(/__name__label__/g, 'Profil : ')
                .replace(/__name__/g));
         
        // Ajout du formulaire de profil à la division profil
        conteneur.append(prototypeForm);
       
    }

})
    