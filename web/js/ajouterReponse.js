$(document).ready(function () {
    
    var idFormulaire = 0;
    
    // Ajout d'une action au clic sur le bouton Ajouter une réponse
    $('#ajouterReponse').click(function (e) {
       
        ajouterReponseForm(idFormulaire);
        idFormulaire++;
       
    });
    
    
    



    function ajouterReponseForm(idFormulaire) {
        
        // Copie du formulaire d'ajout de réponse caché dans le source html
        var divReponse = $('#reponseModele').clone();
        
        // Attributs de la div globale
        divReponse.removeAttr('hidden');
        divReponse.attr('id', 'divReponse' + idFormulaire);
        
        // Attributs de la zone de texte de la réponse
        var divTextarea = divReponse.children('#textareaModele');
        divTextarea.attr('id', 'divTextarea' + idFormulaire);
        divTextarea.children('textarea').attr('id', 'reponse' + idFormulaire);
        divTextarea.children('textarea').attr('name', 'reponse' + idFormulaire);
        divTextarea.children('textarea').attr('required', 'required');
        divTextarea.children('label').attr('for', 'reponse' + idFormulaire);
        
        // Attributs de la checkbox
        var divCheckbox = divReponse.children('#checkboxModele');
        divCheckbox.attr('id', 'divCheckbox' + idFormulaire);
        divCheckbox.children('input').attr('id', 'reponseCorrecte' + idFormulaire);
        divCheckbox.children('input').attr('name', 'reponseCorrecte' + idFormulaire);
        divCheckbox.children('label').attr('for', 'reponseCorrecte' + idFormulaire);
        
        // Attributs du bouton de suppression
        var boutonSupprimer = divReponse.children('input');
        boutonSupprimer.attr('id', 'supprimerReponse' + idFormulaire);
        
        boutonSupprimer.click(function (e) {
           divReponse.remove(); 
        });
        
        // Insertion du nouveau formulaire 
        divReponse.insertBefore($('#ajouterReponse'));
        
    }

})