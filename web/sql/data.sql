--
-- Contenu de la table inscription
--

INSERT INTO inscription (id, dureeValidite, tempsEcoule, etat, resultat, candidat, creerPar, test) VALUES
	(27, 1392550000,       0, 'NOTSTARTED', NULL, 62, 62, 29),
	(30, 1392560000, 7226.38,   'FINISHED',    1, 65, 62, 29),
	(31, 1392720000, 40.9956,   'FINISHED',    0, 65, 62, 30),
	(32, 1392720000, 761.508,   'FINISHED',    1, 65, 62, 30),
	(33, 1392720000, 14.0379,   'FINISHED',  0.5, 65, 62, 30),
	(34, 1392730000, 18.0693,   'FINISHED',    0, 65, 62, 30),
	(35, 1392730000, 25.0546,   'FINISHED',    1, 65, 62, 29),
	(36, 1392730000, 59.9041,   'FINISHED',    1, 65, 62, 30);

--
-- Contenu de la table promotion
--

INSERT INTO promotion (codePromo, libelle) VALUES
	('CDI7', 'Concepteur développeur informatique, groupe 7'),
	('CDI8', 'Concepteur développeur informatique, groupe 8'),
	('CDI9', 'Concepteur développeur informatique, groupe 9');

--
-- Contenu de la table question
--

INSERT INTO question (id, enonce, media, type, theme) VALUES
	(1, 'Quel est le mot clé pour créer une variable en PHP?', 					'Questions/1.jpg', 	0, 1),
	(2, 'Question de test 2', 													'', 				1, 4),
	(3, 'Question un peu naze', 												'', 				1, 4),
	(5, 'Comment sont encapsulés les attributs de classe en PHP par defaut?',	'Questions/5.webm', 0, 1),
	(6, 'Comment peut-on créer un tableau en PHP? (PHP 5.4)', 					'', 				1, 1),
	(7, 'Si $a=6; $b=$a++; et $c=++$a; Combien valent $b et $c ?', 				'', 				0, 1),
	(8, 'Quel est l''utilité de echo?', 										'', 				1, 1),
	(9, 'Qu''est-ce qu''un array?', 											'', 				1, 1),
	(10, 'Comment réaliser une concatenation en php?', 							'', 				0, 1);

--
-- Contenu de la table question_tirage
--

INSERT INTO question_tirage (inscription, question, estMarquee) VALUES
	(27,  2, 0),
	(27,  3, 0),
	(27,  5, 0),
	(30,  1, 0),
	(30,  2, 0),
	(30,  3, 0),
	(31,  1, 0),
	(31,  5, 0),
	(32,  1, 0),
	(32,  5, 0),
	(33,  1, 1),
	(33,  5, 1),
	(34,  1, 0),
	(34,  5, 0),
	(35,  1, 0),
	(35,  2, 0),
	(35,  3, 0),
	(35,  5, 0),
	(36,  5, 0),
	(36,  6, 0),
	(36,  7, 0),
	(36,  8, 0),
	(36,  9, 0),
	(36, 10, 0);

--
-- Contenu de la table reponse_donnee
--

INSERT INTO reponse_donnee (id, reponse_proposee, question, inscription) VALUES
	(37, 135,  3, 30),
	(56, 135,  3, 35),
	(55, 164,  2, 35),
	(59, 228,  5, 36),
	(60, 235,  6, 36),
	(61, 236,  6, 36),
	(62, 237,  6, 36),
	(63, 241,  7, 36),
	(64, 247,  8, 36),
	(65, 249,  9, 36),
	(66, 253, 10, 36);

--
-- Contenu de la table reponse_proposee
--

INSERT INTO reponse_proposee (id, enonce, estBonne, question) VALUES
	(135, 'Le gras c''est la vie', 								1,  3),
	(136, 'Pays de gal indépendant', 							0,  3),
	(163, 'Ma réponse est simple', 								0,  2),
	(164, 'Ma réponse est complexe', 							1,  2),
	(227, 'protected', 											0,  5),
	(228, 'public', 											1,  5),
	(229, 'private', 											0,  5),
	(230, 'friend', 											0,  5),
	(231, '$', 													1,  1),
	(232, 'var', 												0,  1),
	(233, 'public', 											0,  1),
	(234, 'Dim', 												0,  1),
	(235, '$tableau = array(''clé''=>''valeur'');', 			1,  6),
	(236, '$tableau=[''clé''=>''valeur''];', 					1,  6),
	(237, '$tableau[''clé'']=''valeur'';', 						1,  6),
	(238, '$tableau=new Array(''clé'', ''valeur'');', 			0,  6),
	(239, '$b==6 at $c==7', 									0,  7),
	(240, '$b==7 at $c==8', 									0,  7),
	(241, '$b==6 at $c==8', 									1,  7),
	(242, '$b==7 at $c==7', 									0,  7),
	(243, '$b==6 at $c==6', 									0,  7),
	(244, 'Répéter plusieurs fois la même action', 				0,  8),
	(245, 'Changer la valeur d''une variable', 					0,  8),
	(246, 'A faire référence à une autre variable', 			0,  8),
	(247, 'Ecrire un message dans le flux de sortie',		 	1,  8),
	(248, 'La définition d''une variable', 						0,  9),
	(249, 'Un tableau', 										1,  9),
	(250, 'Une fonction permettant la création d''une classe', 	0,  9),
	(251, 'Une gamme', 											0,  9),
	(252, '$var = ''test''+'' 1'';', 							0, 10),
	(253, '$var = ''test''.'' 1'';', 							1, 10),
	(254, '$var = ''test''.concat('' 1'');', 					0, 10),
	(255, '$var = newString(''test'').concat('' 1'');', 		0, 10);

--
-- Contenu de la table section_test
--

INSERT INTO section_test (test, theme, nbQuestions) VALUES
	(29, 1, 2),
	(29, 4, 2),
	(30, 1, 6);

--
-- Contenu de la table test
--

INSERT INTO test (id, libelle, description, duree, seuil, creer) VALUES
	(29, 'mon superbe test sur les interfaces WEB', 'Vous allez aimer le WEB', 	4, 6, 62),
	(30, 'test sur le php', 						'Deuxi&egrave;me test', 	2, 6, 62);

--
-- Contenu de la table theme
--

INSERT INTO theme (id, libelle) VALUES
	(1, 'Le développement en PHP'	),
	(4, 'Les tests unitaires'		);

--
-- Contenu de la table profile
--

INSERT INTO profil (id, isAdmin, isFormateur, isRespFormateur, isCandidat, isCellRecrut, 
						username, username_canonical, email, email_canonical, enabled, salt, password, locked, expired, 
						roles, credentials_expired) VALUES 
	(25, 1, 0, 0, 0, 0, 'AlphonseBC', 'AlphonseBC', 'capriani.alphonse@gmail.com', 'capriani.alphonse@gmail.com', 1, 1, sha1('BigCap'), 1, 1, '', 1);
;


	
	
--
-- Contenu de la table utilisateur
--

INSERT INTO utilisateur (id, nom, prenom, dateNaissance, adresse, codePostal, ville, idProfile) VALUES 
	(62, 'Capriani', 	'Alphonse', '1960-02-27', '245 4th Street', 			'10110', 'New York',		25), 
	(65, 'Rizzo', 		'Gordon', 	'1976-01-30', '423 21st Street', 			'10110', 'New York',		26),
	(66, 'Davis', 		'Jessie', 	'1978-09-14', '156 Figueroa Street', 		'22455', 'Los Angeles',		27),
	(68, 'Tejera', 		'Joaquin', 	'1977-11-12', '8 Great Town Street', 		'75574', 'El Paso',			38),
	(70, 'Heavyballs', 	'Michael', 	'1964-10-02', '12 Fitzgerald Avenue', 		'55412', 'Baltimore',		40),
	(71, 'Caisse', 		'Jean', 	'1982-01-05', '12 rue du maréchal Joffre', 	'44800', 'Saint Herblain',	45)
;
	
	


INSERT INTO utilisateur (id, nom, prenom, email, password, level, codePromotion) VALUES
	(62, 'default', 'default', 'default@default.com', 'f0738d89e307439041cf706eddad114383713b7ac3ec58e47c84fb76ba6eb8308551fa4320d7c7184aec96ac47186d0959e4819e250200d5511316d1e61ee2bb', '2', NULL),
	(65, 'Balan', 'David', 'david@balandavid.com', 'a768df43ae5bf9304277111d9460eed1f79e64a59ad2ffbda6f0a89331a8a4d2247e4e4e192881ca6c3ca00a4d22dc367e78ec545cbbaa934b7880fdac3df7f3', '1', 'CDI7');

