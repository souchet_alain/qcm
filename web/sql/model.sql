-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mer 29 Janvier 2014 à 15:11
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données: `qcm`
--
CREATE DATABASE IF NOT EXISTS `qcm` DEFAULT CHARACTER SET utf8;
USE `qcm`;

-- --------------------------------------------------------

--
-- Structure de la table `inscription`
--

CREATE TABLE IF NOT EXISTS `inscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dureeValidite` float NOT NULL,
  `tempsEcoule` float NOT NULL,
  `etat` varchar(12) NOT NULL,
  `resultat` float DEFAULT NULL,
  `candidat` int(11) NOT NULL,
  `creerPar` int(11) NOT NULL,
  `test` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `creerPar` (`creerPar`),
  KEY `candidat` (`candidat`),
  KEY `test` (`test`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

-- --------------------------------------------------------

--
-- Structure de la table `promotion`
--

CREATE TABLE IF NOT EXISTS `promotion` (
  `codePromo` varchar(10) NOT NULL,
  `libelle` varchar(128) NOT NULL,
  PRIMARY KEY (`codePromo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Structure de la table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enonce` varchar(255) NOT NULL,
  `media` varchar(64),
  `type` tinyint(1) NOT NULL,
  `theme` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `theme` (`theme`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Structure de la table `question_tirage`
--

CREATE TABLE IF NOT EXISTS `question_tirage` (
    id int(11) NOT NULL AUTO_INCREMENT, 
  `inscription` int(11) NOT NULL,
  `question` int(11) NOT NULL,
  `estMarquee` tinyint(1) NOT NULL,
  PRIMARY KEY (id),
  KEY `question` (`question`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Structure de la table `reponse_donnee`
--

CREATE TABLE IF NOT EXISTS `reponse_donnee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reponse_proposee` int(11) NOT NULL,
  `question` int(11) NOT NULL,
  `inscription` int(11) NOT NULL,
  PRIMARY KEY (`id`,`reponse_proposee`,`question`,`inscription`),
  KEY `reponse_proposee` (`reponse_proposee`),
  KEY `question` (`question`),
  KEY `inscription` (`inscription`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=67 ;

--
-- Structure de la table `reponse_proposee`
--

CREATE TABLE IF NOT EXISTS `reponse_proposee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enonce` varchar(512) NOT NULL,
  `estBonne` tinyint(1) NOT NULL,
  `question` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `question` (`question`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=256 ;

--
-- Structure de la table `section_test`
--

CREATE TABLE IF NOT EXISTS `section_test` (
  id	int(11) NOT NULL AUTO_INCREMENT, 
  `test` int(11) NOT NULL,
  `theme` int(11) NOT NULL,
  `nbQuestions` int(11) NOT NULL,
  PRIMARY KEY (id),
  KEY `theme` (`theme`), 
  KEY test (test)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Structure de la table `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(128) NOT NULL,
  `description` varchar(512) NOT NULL,
  `duree` float NOT NULL,
  `seuil` int(11) NOT NULL,
  `creer` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `creer` (`creer`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Structure de la table `theme`
--

CREATE TABLE IF NOT EXISTS `theme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Structure de la table `profil`
--

CREATE TABLE IF NOT EXISTS profil (
	id				int(11)		NOT NULL AUTO_INCREMENT, 
	isAdmin			tinyint		NOT NULL, 
	isFormateur		tinyint		NOT NULL, 
	isRespFormateur	tinyint		NOT NULL, 
	isCandidat		tinyint		NOT NULL, 
	isCellRecrut	tinyint		NOT NULL, 
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=66;

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS utilisateur (
	id				int(11)			NOT NULL AUTO_INCREMENT, 
	nom				varchar(128)	NOT NULL, 
	prenom			varchar(128)	NOT NULL, 
	dateNaissance	datetime		DEFAULT NULL, 
	adresse			varchar(128)	DEFAULT NULL, 
	codePostal		char(5)			DEFAULT NULL, 
	ville			varchar(128)	DEFAULT NULL, 
	codePromotion	varchar(10)		DEFAULT NULL, 
	profil			int(11)			NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=66;


-- ==================== CONTRAINTES ==================== --

--
-- Contraintes pour la table `inscription`
--
ALTER TABLE `inscription`
  ADD CONSTRAINT `inscription_ibfk_1` FOREIGN KEY (`candidat`) REFERENCES `utilisateur` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inscription_ibfk_2` FOREIGN KEY (`creerPar`) REFERENCES `utilisateur` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inscription_ibfk_3` FOREIGN KEY (`test`) REFERENCES `test` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `question_ibfk_1` FOREIGN KEY (`theme`) REFERENCES `theme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `question_tirage`
--
ALTER TABLE `question_tirage`
  ADD CONSTRAINT `question_tirage_ibfk_1` FOREIGN KEY (`inscription`) REFERENCES `inscription` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `question_tirage_ibfk_2` FOREIGN KEY (`question`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `reponse_donnee`
--
ALTER TABLE `reponse_donnee`
  ADD CONSTRAINT `reponse_donnee_ibfk_1` FOREIGN KEY (`reponse_proposee`) REFERENCES `reponse_proposee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reponse_donnee_ibfk_2` FOREIGN KEY (`question`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reponse_donnee_ibfk_3` FOREIGN KEY (`inscription`) REFERENCES `inscription` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `reponse_proposee`
--
ALTER TABLE `reponse_proposee`
  ADD CONSTRAINT `reponse_proposee_ibfk_1` FOREIGN KEY (`question`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `section_test`
--
ALTER TABLE `section_test`
  ADD CONSTRAINT `section_test_ibfk_1` FOREIGN KEY (`test`) REFERENCES `test` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `section_test_ibfk_2` FOREIGN KEY (`theme`) REFERENCES `theme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `test`
--
ALTER TABLE `test`
  ADD CONSTRAINT `test_ibfk_1` FOREIGN KEY (`creer`) REFERENCES `utilisateur` (`id`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT utilisateur_ibfk_1 FOREIGN KEY (codePromotion) REFERENCES promotion (codePromo) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT utilisateur_ibfk_2 FOREIGN KEY (profil) REFERENCES profil (id) ON DELETE CASCADE ON UPDATE CASCADE;


